import Router from "@koa/router";
import md5 from "blueimp-md5";
import { createCanvas, Image, loadImage } from "canvas";
import fs from "fs";

const sendfile = require("koa-sendfile");

const router = new Router();

interface Questionnaire {
  questionnaire: Array<{ label: string; count: number; remark: string }>;
  remark: string;
}

interface SigntrueData {
  name: string;
  gender: string;
  relation: string;
  template: string;
  idCard?: string;
  phone?: string;
  signetur?: {
    width: number;
    height: number;
    lines: Array<{ x: number; y: number }>[];
  };
  relationIdCard?: string;
  relationPhone?: string;
  relationSignetur?: {
    width: number;
    height: number;
    lines: Array<{ x: number; y: number }>[];
  };
  questionnaire?: Questionnaire;
}

router.post("/signtrue", async (ctx) => {
  const data = ctx.request.body;
  const filenames: any = {
    1: {
      filename: "致客户书.jpg",
      fn: draw1,
    },
    2: {
      filename: "放射肿瘤医学咨询服务确认书-1.jpg",
      fn: draw2,
    },
    3: {
      filename: "放射肿瘤医学咨询服务确认书-2.jpg",
      fn: draw2,
    },
    4: {
      filename: "放射肿瘤医学咨询服务确认书-3.jpg",
      fn: draw2,
    },
    5: {
      filename: "放射肿瘤医学咨询客户服务满意度问卷.jpg",
      fn: draw3,
    },
  };

  const template = filenames[data.template];

  const img = await loadImage(`${__dirname}/assets/img/${template.filename}`);
  const canvas = createCanvas(img.width, img.height);
  const ctx2d = canvas.getContext("2d");

  ctx2d.drawImage(img, 0, 0);

  template.fn(ctx2d, data);

  const mimeType = "image/jpeg";
  const writeFileName = `${md5(JSON.stringify(data))}.jpeg`;
  const path = `${__dirname}/assets/temp/${writeFileName}`;

  const buffer = await canvas.toBuffer(mimeType, {
    quality: 0.75,
    progressive: false,
    chromaSubsampling: true,
  });

  await fs.writeFileSync(path, buffer);

  ctx.body = {
    success: true,
    data: writeFileName,
  };
});

router.get("/assets/image/:filename", async (ctx) => {
  await sendfile(ctx, `${__dirname}/assets/temp/${ctx.params.filename}`);
});

router.get("/assets/img/:filename", async (ctx) => {
  await sendfile(ctx, `${__dirname}/assets/img/${ctx.params.filename}`);
});

export default router;

function draw1(ctx: any, data: SigntrueData) {
  ctx.font = "24px serif";

  // 客户姓名
  ctx.fillText(`${data.name} ${data.gender}`, 210, 215);

  if (data.relationIdCard) {
    // 授权人于客户关系
    ctx.fillText(data.relation, 790, 1415);
    // 授权人身份证
    ctx.fillText(data.relationIdCard, 250, 1470);
    // 授权人手机号
    ctx.fillText(data.relationPhone, 670, 1470);
  } else {
    // 客户身份证
    ctx.fillText(data.idCard, 250, 1330);
    // 客户手机号
    ctx.fillText(data.phone, 670, 1330);
  }
  if (data.signetur) {
    signetur(ctx, data.signetur, 400, 1240);
  } else if (data.relationSignetur) {
    signetur(ctx, data.relationSignetur, 560, 1370);
  }
  // 签名

  const date = new Date();
  // 年
  ctx.fillText(String(date.getFullYear()), 250, 1520);
  // 月
  ctx.fillText(String(date.getMonth() + 1), 410, 1520);
  // 日
  ctx.fillText(String(date.getDate()), 500, 1520);
}
function draw2(ctx: any, data: SigntrueData) {
  ctx.font = "24px serif";

  // 客户姓名
  ctx.fillText(`${data.name} ${data.gender}`, 210, 300);

  if (data.relationIdCard) {
    // 授权人于客户关系
    ctx.fillText(data.relation, 790, 1380);
    // 授权人身份证
    ctx.fillText(data.relationIdCard, 255, 1440);
    // 授权人手机号
    ctx.fillText(data.relationPhone, 670, 1440);
  } else {
    // 客户身份证
    ctx.fillText(data.idCard, 255, 1255);
    // 客户手机号
    ctx.fillText(data.phone, 670, 1255);
  }
  if (data.signetur) {
    // 客户签名
    signetur(ctx, data.signetur, 400, 1160);
  } else if (data.relationSignetur) {
    // 授权人签名
    signetur(ctx, data.relationSignetur, 560, 1330);
  }
  // 签名

  const date = new Date();
  // 年
  ctx.fillText(String(date.getFullYear()), 250, 1505);
  // 月
  ctx.fillText(String(date.getMonth() + 1), 415, 1505);
  // 日
  ctx.fillText(String(date.getDate()), 510, 1505);
}
function draw3(ctx: any, data: SigntrueData) {
  ctx.font = "24px serif";
  const { questionnaire } = data;
  // 客户姓名
  ctx.fillText(`${data.name} ${data.gender}`, 210, 300);

  const textStart = {
    x: 282,
    y: 542,
  };

  const span = {
    x: 138,
    y: 95,
  };

  questionnaire?.questionnaire.forEach((item, i) => {
    if (item.count < 4 && item.remark) {
      ctx.fillText(`意见：${item.remark}`, 192, 590 + span.y * (i + 1));
    }

    if (item.count > 0) {
      ctx.beginPath();
      ctx.arc(
        textStart.x + span.x * item.count,
        textStart.y + span.y * (i + 1),
        20,
        0,
        Math.PI * 2,
        true
      );

      ctx.stroke();
    }
  });
  if (questionnaire?.remark) {
    ctx.fillText(`${questionnaire?.remark}`, 195, 1250);
  }

  if (data.signetur) {
    // 客户签名
    signetur(ctx, data.signetur, 575, 1350);
    // 客户手机号
    ctx.fillText(data.phone, 225, 1450);
  } else if (data.relationSignetur) {
    // 授权人签名
    signetur(ctx, data.relationSignetur, 575, 1350);
    // 授权人手机号
    ctx.fillText(data.relationPhone, 225, 1450);
  }

  const date = new Date();
  // 年
  ctx.fillText(String(date.getFullYear()), 250, 1495);
  // 月
  ctx.fillText(String(date.getMonth() + 1), 415, 1495);
  // 日
  ctx.fillText(String(date.getDate()), 510, 1495);
}

function signetur(ctx: any, data: any, x: number, y: number) {
  ctx.save();

  const width = data.width;
  const scale = 80 / width;
  const start = {
    x,
    y,
  };
  ctx.translate(start.x, start.y);
  ctx.rotate(-(90 * Math.PI) / 180);
  ctx.scale(scale, scale);

  data.lines.forEach((lines: any) => {
    ctx.beginPath();

    ctx.lineWidth = 8;

    lines.forEach((point: any) => {
      ctx.lineTo(-start.x + point.x, -start.y + point.y);
    });

    ctx.stroke();
  });

  ctx.restore();
}
