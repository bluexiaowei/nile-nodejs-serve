import router from "./router";
import cors from "@koa/cors";
import Koa from "koa";
import bodyParser from "koa-bodyparser";

const app = new Koa();

app
  .use(bodyParser())
  .use(cors())
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(3001, () => {
  console.log('::3001')
});
